from flask import Flask
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:admin@localhost/cmpe273'
db = SQLAlchemy(app)


class Manager(db.Model):
    __tablename__ = 'manager'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(50))
    email=db.Column('email',db.String(50))
    category=db.Column('category',db.String(100))
    description=db.Column('description',db.String(150))
    estimated_costs=db.Column('estimated_costs',db.String(50))
    link=db.Column('link',db.String(100))
    submit_date=db.Column('submit_date',db.String(100))
    status=db.Column('status',db.String(60))
    decision_date=db.Column('decision_date',db.String(50))

    def __init__(self, name, email, category, description, estimated_costs, link, submit_date, status, decision_date):
        self.name=name
        self.email=email
        self.category=category
        self.description=description
        self.estimated_costs=estimated_costs
        self.link=link
        self.submit_date=submit_date
        self.status=status
        self.decision_date=decision_date


class CreateDB():
    def __init__(self):
        import sqlalchemy
        engine = sqlalchemy.create_engine('mysql://%s:%s@%s/%s'%('root','admin', 'localhost','cmpe273'))
        engine.execute("CREATE DATABASE IF NOT EXISTS %s"%('cmpe273'))