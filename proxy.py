import socket
import select
import time
import sys
from ServerRegister import ServerRegister
from cbreaker import cirbreak


buffer_size = 4096
delay = 0.0001
forward_to = ('127.0.0.1', 5001)
host = '127.0.0.1'

class Forward:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self, host, port):
        try:
            self.forward.connect((host, port))
            return self.forward
        except Exception, e:
            return False

class TheServer:
    
    input_list = []
    channel = {}

    registrar = ServerRegister()
    serverports = [5001,5002,5003]
    failurecounts = [0,0,0]
    skipflags = [False, False, False]
    deregisterd = [False, False, False]
    pointer = 0


    cbreaker1 = cirbreak()
    cbreaker2 = cirbreak()
    cbreaker3 = cirbreak()
    cbreakers = [cbreaker1, cbreaker2, cbreaker3]
    

    def handleFailure(self, port):
        cb =  self.cbreakers[self.serverports.index(port)]
        cb.handle_failure()

    def deregister(self, port):
        if (self.deregisterd[self.serverports.index(port)] == False):
            self.registrar.widrawServer(self.serverports.index(port)+1)
            self.deregisterd[self.serverports.index(port)] = True
            print "\n\n**************  REMOVING FROM REDIS ROUTING TABLE ************** >>",port

    def skipPort(self,port):
        self.skipflags[self.serverports.index(port)] = True

    def getSkipStatus(self, port):
        return self.skipflags[self.serverports.index(port)] 

    def fetchPort(self):
        port = self.serverports[self.pointer]


        if (port == 5001):
            if self.cbreaker1.check_state() == 0:
                self.deregister(5001)
                if self.cbreaker2.check_state() == 0:
                    self.deregister(5002)
                    if self.cbreaker3.check_state() == 0:
                        self.deregister(5003)
                        return -1
                    else:
                        port = 5003
                        self.pointer = 3
                else:
                    port = 5002
                    self.pointer = 2
            else:
                port = 5001
                self.pointer = 1
        elif (port == 5002):
            if self.cbreaker2.check_state() == 0:
                self.deregister(5002)
                if self.cbreaker3.check_state() ==0:
                    self.deregister(5003)
                    if self.cbreaker1.check_state() ==0:
                        self.deregister(5001)
                        return -1
                    else:
                        port = 5001
                        self.pointer=1
                else:
                    port = 5003
                    self.pointer = 3
            else:
                port= 5002
                self.pointer = 2
        elif (port == 5003):
            if self.cbreaker3.check_state() == 0:
                self.deregister(5003)
                if self.cbreaker1.check_state() ==0:
                    self.deregister(5001)
                    if self.cbreaker2.check_state() ==0:
                        self.deregister(5002)
                        return -1
                    else:
                        port = 5002
                        self.pointer=2
                else:
                    port = 5001
                    self.pointer = 1
            else:
                port= 5003
                self.pointer = 3
            

        if self.pointer == 3:
            self.pointer = 0
        return port
    

    def getPortFailureCount(self,port):
        pfcount = self.failurecounts[self.serverports.index(port)]
        return pfcount


    def updateFailureCount(self,port):
         pfcount = self.failurecounts[self.serverports.index(port)]
         pfcount += 1
         self.failurecounts[self.serverports.index(port)] = pfcount
         self.handleFailure(port)


    def __init__(self, host, port):
        
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(200)


    def main_loop(self):
        self.input_list.append(self.server)
        while 1:
            time.sleep(delay)
            ss = select.select
            inputready, outputready, exceptready = ss(self.input_list, [], [])
            for self.s in inputready:
                if self.s == self.server:
                    self.on_accept()
                    break

                self.data = self.s.recv(buffer_size)
                if len(self.data) == 0:
                    self.on_close()
                    break
                else:
                    self.on_recv()

    def on_accept(self):
        port = self.fetchPort()
        if(port == -1):
            print "\n**************  NO ACTIVE SERVERS FOUND | Closing Router... **************  >>"
            sys.exit()


        forward = Forward().start(host,port)
        clientsock, clientaddr = self.server.accept()

        if forward:
            print "**************  CONNECTED TO **************  >>",port
            self.input_list.append(clientsock)
            self.deregisterd[self.serverports.index(port)] = False
            self.input_list.append(forward)
            self.channel[clientsock] = forward
            self.channel[forward] = clientsock
        else: 
            print "**************  FAILED TO CONNECT TO **************  >>",port
            self.updateFailureCount(port)
            if(self.getPortFailureCount(port)>3):
                self.skipPort(port)
            clientsock.close()

    def on_close(self):

        self.input_list.remove(self.s)
        self.input_list.remove(self.channel[self.s])
        out = self.channel[self.s]
        self.channel[out].close()
        self.channel[self.s].close()
        del self.channel[out]
        del self.channel[self.s]

    def on_recv(self):
        data = self.data
        print data
        self.channel[self.s].send(data)

if __name__ == '__main__':
        server = TheServer('', 9090)
        try:
            server.main_loop()
        except KeyboardInterrupt:
            print "Ctrl C - Stopping server"
            sys.exit(1)