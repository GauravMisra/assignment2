import redis

class ServerRegister:

    def __init__(self):
       self.r = redis.Redis(host='127.0.0.1',port=6379,password=None)
     
    def registerServer(self,key,value):
        self.r.set(key,value)
    
    def widrawServer(self,key):
        self.r.delete(key)
    
    def fetchServer(self,key):
        return self.r.get(key)


